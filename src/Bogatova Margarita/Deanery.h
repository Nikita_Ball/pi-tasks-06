#pragma once
#include <fstream>
#include <string>
#include <iostream>
#include "Group.h"
#include <time.h>
using namespace std;
class Deanery
{
	Student **st;
	Group **gr;
	int numSt;
	int numGr;
public:
	Deanery();
	~Deanery();
	int loadGroups(string fileName);
	int loadStudent(string fileName);
	int findGroup(int id);
	void findExcellentSt(int TitleGr);
	void PrintAvMarkGroup();
	void setHead();
	void PrintHead(int TitleGr);
	void AddRandMark(int id);
	void DeleteBadStudent();
	void TransferStudent(int id, int TitleGr);
	void findBestStudent();
	void findBestGroup();
	void PrintStudent(int id);
	int SaveFile(string fileName);
};
