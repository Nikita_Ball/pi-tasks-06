#include <Windows.h>
#include <iostream>
#include <string>

#include "Student.h"
#include "Group.h"
#include "Dekanat.h"

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	
	Dekanat * Dk = new Dekanat();
	Dk->createGroupList("group.txt");
	Dk->createStudList("student.txt");
	
	Dk->PrintGroups();
	Dk->PrintStud();

	Dk->choiseHeadForAll();

	Dk->addRNDMarkForAllStud();
	Dk->AvarageStudRating();
	Dk->AvarageGroupRating();

	Dk->checkProgress();

	Dk->addRNDMarkForAllStud();
	Dk->AvarageStudRating();
	Dk->AvarageGroupRating();

	Dk->checkProgress();

	Dk->transferStud(Dk->rndStud(), Dk->rndGroup());
	Dk->transferStud(Dk->rndStud(), Dk->rndGroup());
	Dk->transferStud(Dk->rndStud(), Dk->rndGroup());
	Dk->transferStud(Dk->rndStud(), Dk->rndGroup());

	Dk->PrintStud();

	delete Dk;

	return 0;
}