#include "dekanat.h"
#include "time.h"


Dekanat::Dekanat()
{
	logs.open("logs.txt");
}
void Dekanat::loadStudents(string path)
{
	logs << "loading students..." << endl;
    ifstream file(path);
    if (!file.is_open())
    {
		logs << "loadingStudents fail" << endl;
        return;
    }
    int num;
    file>>num;
    for (int i =0; i<num;i++)
    {
        int id;
        string fio,s1,s2,s3;
        file>>id;
        file>>s1>>s2>>s3;
        fio = s1 + " " + s2 + " " + s3;
        st.push_back(Student(id,fio));
		logs << st[st.size() - 1].getId() << " " << st[st.size() - 1].getFio() << endl;

    }
	logs << "Students loaded" << endl;
		
    file.close();
}

void Dekanat::distributeToGroups()
{
    int n  = st.size()/gr.size();
    for(int i = 0; i < gr.size();i++)
    {
        for (int j=0;j<n;j++)
        {
            gr[i].addStudent(&st[j+i*n]);
        }
    }
	logs << "students distributed to groups";
}

void Dekanat::print()
{
    for(int i=0; i < gr.size(); i++)
    {
        gr[i].print();
    }
}

void Dekanat::printTheBest()
{
	cout << "The best group " << bestGroup()->getTitle() << endl;
	cout << "The best student " << bestStudent()->getFio() << endl;
	for (size_t i = 0; i < gr.size(); i++)
	{
		cout << "the best student in " << gr[i].getTitle()\
			<< "is " << gr[i].bestStudent()->getFio()<<endl;
	}
}

void Dekanat::printHeads()
{
	for (size_t i = 0; i < gr.size(); i++)
	{
		cout << gr[i].getTitle() << "Head: id: "\
			<< gr[i].getHead()->getId() << " fio: " << gr[i].getHead()->getFio() << endl;
	}
}

void Dekanat::printStatisticGroups()
{
	cout << "avaradge marks:" << endl;
	for (size_t i = 0; i < gr.size(); i++)
	{
		cout <<gr[i].getTitle()<<" "<< gr[i].getAvMark() << endl;
	}
}

void Dekanat::printStatisticStudents()
{
	cout << "avaradge marks:" << endl;
	for (size_t i = 0; i < st.size(); i++)
	{
		cout << st[i].getId()<<" "<<st[i].getFio() << " " << st[i].getAvMark() << endl;
	}
}

void Dekanat::setMarks()
{
    for (int i = 0; i < gr.size(); i++)
    {
        gr[i].setMarks(3);//каждому студенту по 3 оценки
    }
	logs << "marks setted" << endl;
}

void Dekanat::removeStud(int id)
{
    int ind = findStud(id);
	if (ind == -1)
	{
		logs << "fail removing" << endl;
		return;
	}

    for (int i = 0; i < gr.size(); i++)
    {
        int indg = gr[i].findStudent(id);
        if (indg != -1)
        {
            gr[i].removeStud(indg);
            break;
        }
    }
	logs << "Student id " << st[ind].getId() << " fio " << st[ind].getFio() << " removed" << endl;
    Student tmp;
    tmp = st[ind];
    st[ind] = st[st.size() - 1];
    st[st.size() - 1] = tmp;
    st.pop_back();

}

void Dekanat::moveStud(int id, string destTitle)
{
	logs << "movinf student id " << id << " to group " << destTitle << endl;
	int index = findStud(id);
	if (index == -1)
	{
		logs << "fail moving student not found" << endl;
		return;
	}
	int indgr = findGroup(destTitle);
	if (indgr == -1)
	{
		logs << "fail moving group not found" << endl;
		return;
	}

	if (st[index].getGroup()->getHead() == &st[index])
	{
		logs << "student id " << id << " was head of group " << st[index].getGroup()->getTitle() << " changing head..." << endl;


		st[index].getGroup()->changeHead();
	}
	logs << "student id " << id << " moved from " << st[index].getGroup()->getTitle() << " to " << destTitle << endl;
	st[index].exitGroup();
	gr[indgr].addStudent(&st[index]);
}

Student * Dekanat::bestStudent()
{
	logs << "searching for the best student" << endl;
	double best = 0;
	Student * res = 0;
	for (int i = 0; i < st.size(); i++)
	{
		if (st[i].getAvMark() >= best)
		{
			best = st[i].getAvMark();
			res = &st[i];
		}
	}
	logs << "the best student is id: " << res->getId() << " fio: " << res->getFio() << endl;
	return res;
}

Group * Dekanat::bestGroup()
{
	logs << "searching for the best group" << endl;
	double best = 0;
	Group* group=0;
	for (size_t i = 0; i < gr.size(); i++)
	{
		if (gr[i].getAvMark() >= best)
		{
			best = gr[i].getAvMark();
			group = &gr[i];
		}
	}
	logs << "the best group is " << group->getTitle() << endl;
	return group;
}

void Dekanat::removeBedStudents()
{
	logs << "removing bed students" << endl;
    for (int i = 0; i < gr.size(); i++)
    {
        int id;
        while ((id=gr[i].getDewager())!= -1)
        {
			logs << "student " << id << " is bed"<<endl;
            gr[i].removeStud(gr[i].findStudent(id));
            removeStud(id);
        }
    }
	logs << "bed students removed" << endl;
}

void Dekanat::nextCourse()
{

}

void Dekanat::saveStud()
{
	logs << "saving students to file" << endl;
    ofstream out("newstud.txt");
	out << st.size() << endl;
    for(int i =0; i < st.size(); i++)
    {
        out<<st[i].getId()<<"\t"<<st[i].getFio()<<endl;
    }
}

void Dekanat::saveGroups()
{
	logs << "saving groups to file" << endl;
	ofstream out("newgroups.txt");
	out << gr.size()<<endl;
	for (size_t i = 0; i < gr.size(); i++)
	{
		out << gr[i].getTitle() << endl;
	}

}

void Dekanat::setHeads()
{
	logs << "setting heads" << endl;
	for (int i = 0; i < gr.size(); i++)
	{
		gr[i].setHead(rand() % gr[i].getStudentsNum());
		logs << "in group " << gr[i].getTitle() << " head is  id " << gr[i].getHead()->getId() << " fio " << gr[i].getHead()->getFio() << endl;
	}
	logs << "heads setted" << endl;
}

int Dekanat::findStud(int id)
{
	logs << "searching for student with id " << id << endl;
    int res = -1;
    for (int i = 0; i < st.size(); i++)
    {
        if (st[i].getId() == id)
        {
			logs << "student found " << st[i].getFio() << endl;
            return i;
        }
    }
	logs << "Student not found" << endl;
    return res;
}

int Dekanat::findGroup(string title)
{
	logs << "searching for group wih title " << title << endl;
	int res = -1;
	for (int i = 0; i < gr.size(); i++)
	{
		if (gr[i].getTitle() == title)
		{
			logs << "group found" << endl;
			return i;
		}
	}
	logs << "group not found" << endl;
	return res;
}


void Dekanat::loadGroups(string path)
{
	logs << "loading groups..." << endl;
    ifstream file(path);
    if (!file.is_open())
    {
		logs << "fail file not open" << endl;
        return;
    }
    int num;
    file>>num;

	for (int i = 0; i < num; i++)
	{
		string s;
		file >> s;
		Group group;
		group.setTitle(s);
		gr.push_back(group);
		logs << "loaded group" << gr[gr.size() - 1].getTitle() << endl;
	}
	logs << "groups loaded" << endl;
}

