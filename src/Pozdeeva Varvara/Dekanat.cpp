#include "Dekanat.h"
#include "Group.h"
#include "Student.h"
#include <fstream>
#include <iostream>
#include<string>
#include <time.h>
using namespace std;
Dekanat::Dekanat()
{
	numG = 0;
	numS = 0;
}

Dekanat::~Dekanat()
{
}

void Dekanat::LoadGroups()
{
	cout << "--------------------------------------------" << endl;
	cout << "������ �����" << endl;
	cout << "--------------------------------------------" << endl;
	ifstream file("Group.txt");
	if (!file.is_open())
		cout << "file not faund";
	while (!file.eof())
	{
		int title;
		file >> title;
		if (numG == 0)
		{
			Groups = new Group*[numG + 1];
			Groups[numG] = new Group(title);
			cout << Groups[numG]->gettitle() << endl;
		}
		else
		{
			Group **tmp = new Group*[numG + 1];
			for (int i = 0; i < numG;i++)
				tmp[i] = Groups[i];
			delete[]Groups;
			Groups = tmp;
			Groups[numG] = new Group(title);
			cout << Groups[numG]->gettitle() << endl;
		}
		numG+=1;
	}
	file.close();
}

void Dekanat::LoadStudents()
{
	cout << "--------------------------------------------" << endl;
	cout << "������ ���������" << endl;
	cout << "--------------------------------------------" << endl;
	ifstream file("Students.txt");
	if (!file.is_open())
		cout << "file not faund";
	while (!file.eof())
	{
		int id;
		string fio, f, n, o;
		int gr;
		file >> id >> f >> n >> o >> gr;
		fio = f + " " + n + " " + o;
		if (numS == 0)
		{
			Students = new Student*[numS + 1];
			Students[numS] = new Student(id,fio);
		}
		else
		{
			Student** tmp = new Student*[numS + 1];
			for (int i = 0; i < numS; i++)
				tmp[i] = Students[i];
			delete[]Students;
			Students = tmp;
			Students[numS] = new Student(id, fio);
		}
		for (int i = 0; i < numG; i++)
		{
			if (gr == Groups[i]->gettitle())
				Groups[i]->AddStud(Students[numS]);
		}
		cout <<Students[numS]->getID()<<" "<< Students[numS]->getFio() << endl;
		numS++;
	}
	file.close();
}

void Dekanat::Addmarks()
{
	cout << "--------------------------------------------" << endl;
	cout << "������" << endl;
	cout << "--------------------------------------------" << endl;
	srand(time(NULL));
	for (int i = 0; i < numS; i++)
	{
		cout << Students[i]->getID()<<" "<<Students[i]->getFio()<<" ";
		Students[i]->AddMarks();
	}
}

void Dekanat::Statistic()
{
	cout << "--------------------------------------------" << endl;
	cout << "������ �������� � ������������ �����" << endl;
	cout << "--------------------------------------------" << endl;
	for (int i = 0; i < numS; i++)
		if (Students[i]->AverMark() == 5)
			cout << "O������� - "<< Students[i]->getFio() << endl;
	for (int i = 0; i < numG; i++)
		cout << "������� ��� � ������ �" << Groups[i]->gettitle() << " - " << Groups[i]->AverMark() << endl;
}

void Dekanat::Deduction()//����������  ��������� �� ��������������

{
	cout << "--------------------------------------------" << endl;
	cout << "����������� ��������" << endl;
	cout << "--------------------------------------------" << endl;
	
	for (int i = 0; i < numS; i++)
	{
		if (Students[i]->badMark())
		{
			int id = Students[i]->getID();
			Students[i]->getGr()->DeletStud(id);
			cout << "������� �������� - " << Students[i]->getFio() << endl;
			Students[i]->setDeduction();
			for (int j = i; j < numS - 1; j++)
				Students[j] = Students[j + 1];
			i--;
			numS--;
		}
	}
}

void Dekanat::ChoiceHead()
{
	cout << "--------------------------------------------" << endl;
	cout << "��������" << endl;
	cout << "--------------------------------------------" << endl;
	for (int i = 0; i < numG; i++)
		Groups[i]->ChoiceHead();
}

void Dekanat::TransferStud()
{
	cout << "--------------------------------------------" << endl;
	cout << "������� ���������" << endl;
	cout << "--------------------------------------------" << endl;
	int id;
	bool flag = true;
	cout << "������� ID ��������, �������� ����� ���������" << endl;
	while (flag)
	{
		cin >> id;
		for (int i = 0; i < numS; i++)
		{
			if ((Students[i]->getID() == id)&& !(Students[i]->getDeduction()))
			{
				cout << "������� ����� ������, ���� ����� ��������� �� ������:" << endl;
				int gr;
				for (int j = 0; j < numG; j++)
				{
					if (Groups[j] != Students[i]->getGr())
						cout << Groups[j]->gettitle() << endl;
				}				
				cin >> gr;
				Students[i]->getGr()->DeletStud(Students[i]->getID());
				for (int j = 0; j < numG; j++)
				{
					if (gr == Groups[j]->gettitle())
					{
						Groups[j]->AddStud(Students[i]);
						cout << "������� " << Students[i]->getFio() << " ��������� � ������ �" << Groups[j]->gettitle() << endl;
					}
				}
				flag = false;
			}
		}
		if (flag)
			cout << "�������� � ����� ������� �� ����������, ���� �� ��� �������� " << endl<<"������� ����� ��� ���"<<endl;
	}
}

void Dekanat::Save()
{
	ofstream newFile("dekanat.txt");
	for (int i = 0; i < numS; i++)
	{
		newFile << "�" << Students[i]->getID() << " "
			<< Students[i]->getFio() << " ";
		int *mark = Students[i]->getMarks();
		for (int j = 0; j < 10; j++)
		{
			newFile << mark[j];
		}
		newFile <<"  ������� ������ - "<<Students[i]->AverMark() << " "
				<< "������ �" << Students[i]->getGr()->gettitle() << endl << endl;
	}
	newFile.close();
}





