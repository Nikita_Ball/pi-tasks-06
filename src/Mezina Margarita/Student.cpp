#include "Student.h"

Student::Student(int id, string fio, Group* group) {
	this->id = id;
	this->fio = fio;
	this->group = group;
}

void Student::SetId(int id) {
	this->id = id;
}

void Student::SetFio(string fio) {
	this->fio = fio;
}

void Student::SetGroup(Group* group) {
	this->group = group;
}

int Student::GetId() {
	return id;
}

string Student::GetFio() {
	return fio;
}

Group* Student::GetGroup() {
	return group;
}

void Student::AddMark(int new_mark) {
	marks.push_back(new_mark);
}

double Student::GetAvMark() {
	double average_mark=0;
	for (int i = 0; i < marks.size(); ++i)
		average_mark += marks[i];
	average_mark /= marks.size();
	return average_mark;
}

Student::~Student() {
	marks.clear();
}
