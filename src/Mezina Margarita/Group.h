#include <vector>
#include "Student.h"
#include <string>
using namespace std;
class Group {
private:
	string title;
	Student* head;
	vector<Student*> staff;
public:
	Group(string title="", Student* head=NULL);
	void SetTitle(string title);
	void SetHead(Student* head);
	string GetTitle();
	Student* GetHead();
	void AddStud(Student*new_student);
	Student* FindStudent(int id);
	void RemoveStudent(Student* stud);
	double GetAvMark();
	~Group();
};

